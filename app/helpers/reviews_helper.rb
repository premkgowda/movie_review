module ReviewsHelper

	def format_average_stars(movie)
    if movie.average_stars.nil?
        'Not Applicable'
     else
        pluralize(movie.average_stars,'star')
    end
  end
end
